import numpy as np 
import matplotlib.pyplot as plt 
import scipy.special as sp

# total sample points
N = 10**5 
# probability of bit 1

p0 = np.arange(start=0,stop = 0.5, step = 0.05)

# Number of points in constellation
M=2 

# all possible input symbols
m = np.arange(0,M) 

# amplitude
A = 1; 

# reference signal constellation
constellation = A*np.cos(m/M*2*np.pi)  

cmap = plt.get_cmap('gnuplot')
colors = [cmap(i) for i in np.linspace(0, 1, 10)]
fig, ax = plt.subplots(nrows=1,ncols = 1)
for k,p_0 in enumerate(p0):
    type(p_0)
    p = 1 - p_0
    snr_dB = np.arange(start=0,stop = 20, step = 1)
    ber_sim = np.zeros(len(snr_dB)) # simulated Bit error rates


    # simulate transmission
    rand_bits = np.random.choice(2, N, p=[p_0, p])
    # map bits to constellation points
    s = constellation[rand_bits] 

    # simulate channel
    for j,snr in enumerate(snr_dB):
        # convert SNR to linear scale
        gamma = 10**(snr/10) 

        # power of the signal
        P = sum(abs(s)**2)/len(s) 

        # spectral density of noise
        N0=P/gamma 

        # complex gaussian noise
        cagwn = (np.random.randn(N) + 1j*np.random.randn(N))*np.sqrt(N0/2)
        # received signal
        r = s + cagwn 
        
        # simulate receiver
        # find the closest constellation point
        received_signal = (r <= 0).astype(int) 
        
        # calculate bit error rate
        ber_sim[j] = np.sum(received_signal != rand_bits)/N 


    theoretical_ber = 0.5*sp.erfc(np.sqrt(10**(snr_dB/10)))
    plt.semilogy(snr_dB,ber_sim,color=colors[k], marker='o', linestyle='', label='p0 = %.2f' % p_0)
    plt.semilogy(snr_dB,theoretical_ber,marker='', linestyle='-')


ax.set_xlabel('$E_b/N_0(dB)$');ax.set_ylabel('BER ($P_b$)')
ax.set_title('Probability of Bit Error for BPSK over CAWGN channel')
ax.set_xlim(0,20);ax.grid(True);
ax.legend();plt.show()
